PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SO-8
$EndINDEX
$MODULE SO-8
Po 0 0 0 15 00000000 00000000 ~~
Li SO-8
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 0.992141 0.218031 0.800114 0.800114 900 0.05 N V 21 "SO-8"
T1 0 0 1 0.9 0 0.05 N H 21 "VAL**"
DS -1.75 -2.5 1.75 -2.5 0.127 21
DS 1.75 -2.5 1.75 2.5 0.127 21
DS 1.75 2.5 -1.75 2.5 0.127 21
DS -1.75 2.5 -1.75 -2.5 0.127 21
DC -1.25 -2 -1 -2 0.127 21
DC -2.5 -2.75 -2.25 -2.75 0.127 21
$PAD
Sh "P$1" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7 -1.905
$EndPAD
$PAD
Sh "P$2" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7 -0.635
$EndPAD
$PAD
Sh "P$3" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7 0.635
$EndPAD
$PAD
Sh "P$4" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7 1.905
$EndPAD
$PAD
Sh "P$5" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.7 1.905
$EndPAD
$PAD
Sh "P$6" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.7 0.635
$EndPAD
$PAD
Sh "P$7" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.7 -0.635
$EndPAD
$PAD
Sh "P$8" R 1.55 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.7 -1.905
$EndPAD
$EndMODULE SO-8
